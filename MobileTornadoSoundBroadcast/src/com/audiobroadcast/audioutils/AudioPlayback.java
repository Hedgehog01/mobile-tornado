package com.audiobroadcast.audioutils;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

public class AudioPlayback {
	
	private static final int BUFFER_SIZE = 4096;
	
	public void play(String FilePath){
		File audioFile = new File (FilePath);
		if (!audioFile.exists()){
			System.out.println("Could not get audio file from path: " + FilePath);
			System.out.println("Closing App!");
			System.exit(0);
		}
		
		try {
			
			AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
			 
            AudioFormat format = audioStream.getFormat();
 
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
 
            SourceDataLine audioLine = (SourceDataLine) AudioSystem.getLine(info);
 
            audioLine.open(format);
 
            audioLine.start();
             
            System.out.println("Playback started.");
             
            byte[] bytesBuffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
 
            while ((bytesRead = audioStream.read(bytesBuffer)) != -1) {
                audioLine.write(bytesBuffer, 0, bytesRead);
            }
             
            audioLine.drain();
            audioLine.close();
            audioStream.close();
             
            System.out.println("Playback completed.");
             
        } catch (UnsupportedAudioFileException ex) {
            System.out.println("The specified audio file is not supported.");
            ex.printStackTrace();
        } catch (LineUnavailableException ex) {
            System.out.println("Audio line for playing back is unavailable.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error playing the audio file.");
            ex.printStackTrace();
        }      
		
		
	}
	

	public static void main(String[] args) {
		
		String audioFilePath = "C:/temp/audio-recordings/1.wav";
		AudioPlayback player = new AudioPlayback();
        player.play(audioFilePath);

	}

}
