package com.audiobroadcast.audioutils;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

public class AudioRecorder {

	private static final String FOLDER_PATH = "C:/temp/audio-recordings/";
	
	//format of the audio file
	AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;
	
	//get audio format
	AudioFormat format = getAudioFormat();
	
	//the line from which audio is recorded
	TargetDataLine line;
	
	/**
	 * method to record audio from PC microphone
	 * @return a String with the path to the audio file
	 */
	public String recordAudio (String fileName){
		File audioRecording = (createAudioFile(FOLDER_PATH,fileName));
		if (audioRecording == null){
			System.out.println("File for recording could not be created...\nExiting Program");
			System.exit(0);
		}

		 DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
		 
         // checks if system supports the data line
         if (!AudioSystem.isLineSupported(info)) {
             System.out.println("Line not supported");
             System.exit(0);
         }
         try {
			line = (TargetDataLine) AudioSystem.getLine(info);
			line.open(format);
			line.start();
			
			 System.out.println("Start capturing...");
			 
	            AudioInputStream ais = new AudioInputStream(line);
	 
	            System.out.println("Start recording...");
	 
	            // start recording
	            AudioSystem.write(ais, fileType, audioRecording);
			
			
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         
         
		
		
		return audioRecording.getAbsolutePath();
	}
	
	public void stopRecording(){
		line.stop();
		line.close();
		System.out.println("Recording stopped");
	}

	private AudioFormat getAudioFormat() {
        float sampleRate = 16000;
        int sampleSizeInBits = 8;
        int channels = 1;
        boolean signed = true;
        boolean bigEndian = true;
        AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits,
                                             channels, signed, bigEndian);
        return format;
    } 

	private File createAudioFile(String folderPath, String fileName) {
		//path to where audio files will be stored
		File audioFilesPath = new File(folderPath);
		
		//check if the path exists
		if (!audioFilesPath.exists()){
			if (audioFilesPath.mkdirs()){
				System.out.println("Folder for audio created successfully at path: " + audioFilesPath.getAbsolutePath());
			}else {
				System.out.println("Folder for audio created was NOT created.\npath should have been " + audioFilesPath.getAbsolutePath());
				System.exit(0);
			}
		}
		
		File audioFile = new File (audioFilesPath.getPath() + File.separator +  fileName);
		
		try {
			if (audioFile.createNewFile()){
				System.out.println("File " + fileName + "created successfuly at:\n"+ audioFile.getAbsolutePath());
				return audioFile;
			}else {
				System.out.println("File " + fileName + "created could not be created at:\n"+ audioFile.getAbsolutePath());
				return null;
			}
		} catch (IOException e) {
			System.out.println("File " + fileName + "could not be created!! exception caught:");
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public static void main(String[] args) {

        final AudioRecorder recorder = new AudioRecorder();
        
        // creates a new thread that waits for a specified
        // of time before stopping
        Thread stopper = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                recorder.stopRecording();
            }
        });
 
        stopper.start();
 
        // start recording
        String filePath = recorder.recordAudio("test.wav");
        System.out.println("File will located at: " + filePath);

	}

}
